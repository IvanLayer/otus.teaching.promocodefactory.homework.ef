﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                id = Guid.Parse("133b71e0-cea8-4bdf-9e80-2e3ae2939720"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Roles = new List<Role>(),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                id = Guid.Parse("1ac86fd9-757b-462f-92e2-8b759bae5f8b"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Roles = new List<Role>(),
                AppliedPromocodesCount = 10
            }
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                id = Guid.Parse("ff763e13-19be-43ce-932a-1ef04f657851"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                id = Guid.Parse("7244d59e-58c1-43db-88ad-4efac55bcd55"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                id = Guid.Parse("de80a351-cd99-4686-928d-7fb852ffa36b"),
                Name = "Театр",
                Customers = new List<Customer>()
            },
            new Preference()
            {
                id = Guid.Parse("dda99f61-173e-479c-bbb0-8a8481733661"),
                Name = "Кино",
                Customers = new List<Customer>()
            }
        };

        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {
            new PromoCode()
            {
                id = Guid.Parse("e747ba1c-0508-453c-9c9a-de82c900e2fe"),
                Code = "Code",
                ServiceInfo = "ServiceInfo",
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(7),
                PartnerName = "PartnerName",
                PartnerManager = null,
                Preference = null,

            }
        };

        public static IEnumerable<Customer> Customers => new List<Customer>()
        {
            new Customer()
            {
                id = Guid.Parse("cf075a65-3da0-4115-b1e2-78620adcf6fb"),
                FirstName = "Вася",
                LastName = "Пупкин",
                Email = "vasya@pupkin.ru",
                Preferences = new List<Preference>(),
                PromoCodes = new List<PromoCode>(),
            },
            new Customer()
            {
                id = Guid.Parse("2bc76e5d-3a83-4cda-82a7-f282be45fccd"),
                FirstName = "Коля",
                LastName = "Лавров",
                Email = "kolya@lavrov.ru",
                Preferences = new List<Preference>(),
                PromoCodes = new List<PromoCode>(),
            }
        };



    }
}