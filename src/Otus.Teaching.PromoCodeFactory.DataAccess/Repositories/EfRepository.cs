﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.EfDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {

        public EfDbContext _efDbContext;
        public EfRepository(EfDbContext efDbContext)
        {
            _efDbContext = efDbContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _efDbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _efDbContext.Set<T>().SingleAsync(o => o.id == id);
        }

        //Добавление элемента
        public async Task AddObjectAsync(T newObject)
        {
            await _efDbContext.Set<T>().AddAsync(newObject);
            await _efDbContext.SaveChangesAsync();
        }

        public async Task UpdateObjectAsync(T updateObject)
        {
            _efDbContext.Set<T>().Update(updateObject);
            await _efDbContext.SaveChangesAsync();
        }

        public async Task DelObjectAsync(Guid id)
        {
            var find = await GetByIdAsync(id);

            _efDbContext.Set<T>().Remove(find);
            await _efDbContext.SaveChangesAsync();
        }

    }
}
