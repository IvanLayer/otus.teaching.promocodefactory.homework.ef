﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Options;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EfDB
{
    public class EfDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }


        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }


        public EfDbContext(DbContextOptions<EfDbContext> dbContextOptions)
            : base(dbContextOptions)
        {

            if (Database.CanConnect())
            {
                Database.Migrate();
            }
            else
            {
                Database.Migrate();

                var role = FakeDataFactory.Roles.ToList();
                AddRange(role);

                var emp = FakeDataFactory.Employees.ToList();

                emp.FirstOrDefault(x => x.id == Guid.Parse("133b71e0-cea8-4bdf-9e80-2e3ae2939720")).Roles.Add(role.FirstOrDefault(x => x.Name == "Admin"));
                emp.FirstOrDefault(x => x.id == Guid.Parse("1ac86fd9-757b-462f-92e2-8b759bae5f8b")).Roles.Add(role.FirstOrDefault(x => x.Name == "PartnerManager"));
                AddRange(emp);

                var pref = FakeDataFactory.Preferences.ToList();
                AddRange(pref);

                var customers = FakeDataFactory.Customers.ToList();

                customers.FirstOrDefault(x => x.id == Guid.Parse("cf075a65-3da0-4115-b1e2-78620adcf6fb")).Preferences.Add(pref.FirstOrDefault(x => x.id == Guid.Parse("dda99f61-173e-479c-bbb0-8a8481733661")));

                customers.FirstOrDefault(x => x.id == Guid.Parse("2bc76e5d-3a83-4cda-82a7-f282be45fccd")).Preferences = pref;

                AddRange(customers);

                SaveChanges();
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Customer>()
                .HasMany(p => p.PromoCodes)
                .WithOne(c => c.Customer)
                .OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(modelBuilder);
        }
    }


}