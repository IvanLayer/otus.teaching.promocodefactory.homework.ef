﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EfDB
{
    public class EfPromoCodes
    {

        private EfDbContext _efDbContext;
        private IRepository<PromoCode> _promoRepository;

        public EfPromoCodes(EfDbContext efDbContext, IRepository<PromoCode> promoRep)
        {
            _efDbContext = efDbContext;
            _promoRepository = promoRep;
        }

        /// <summary>
        /// Создать промокод и выдать промокод клиенту с указанными предпочтениями
        /// </summary>
        /// <param name="newObject"></param>
        /// <returns></returns>
        public async Task CreateAndGivePromocodeAsync(PromoCode newPromo)
        {

            //Найти сотрудника с ролями Partner Manager и указать ответственным за промокод
            var managersEmployees = _efDbContext.Roles.Include(e => e.Employees).Where(r => r.Name == "PartnerManager").Select(x => x.Employees);

            newPromo.BeginDate = DateTime.Today;
            newPromo.EndDate = DateTime.Today.AddDays(7);
            newPromo.PartnerManager = managersEmployees.First().First();

            await _promoRepository.AddObjectAsync(newPromo);


            // Находим клиентов с указанными предпочтениями
            // выдаем первому найденому клиенту новый промокод
            var customers = _efDbContext.Preferences
                .Include(c => c.Customers)
                .ThenInclude(c => c.PromoCodes)
                .Where(c => c.Name == newPromo.Preference.Name)
                .Select(x => x.Customers);
            

            var first = customers.FirstOrDefault();

            if (first != null)
            {
                first.First().PromoCodes.Add(newPromo);
            }

            _efDbContext.SaveChanges();

        }


    }
}
