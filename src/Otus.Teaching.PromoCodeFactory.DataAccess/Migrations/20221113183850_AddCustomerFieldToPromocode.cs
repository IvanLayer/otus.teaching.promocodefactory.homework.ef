﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddCustomerFieldToPromocode : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PromoCodes_Customers_Customerid",
                table: "PromoCodes");

            migrationBuilder.AddForeignKey(
                name: "FK_PromoCodes_Customers_Customerid",
                table: "PromoCodes",
                column: "Customerid",
                principalTable: "Customers",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PromoCodes_Customers_Customerid",
                table: "PromoCodes");

            migrationBuilder.AddForeignKey(
                name: "FK_PromoCodes_Customers_Customerid",
                table: "PromoCodes",
                column: "Customerid",
                principalTable: "Customers",
                principalColumn: "id");
        }
    }
}
