﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(100)]
        public string FirstName { get; set; }
        [MaxLength(100)] 
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(100)] 
        public string Email { get; set; }

        // Списки Preferences и Promocodes 
        public virtual List<Preference> Preferences { get; set; }
        public virtual List<PromoCode> PromoCodes { get; set; }

    }
}