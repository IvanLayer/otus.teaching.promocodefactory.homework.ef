﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EfDB;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        IRepository<Preference> _prefRepository;
        IRepository<PromoCode> _promoCodeRepository;
        IRepository<Role> _roleRepository;
        EfPromoCodes _efPromo;

        public PromocodesController(IRepository<PromoCode> repP, IRepository<Role> repRole, EfPromoCodes efPromo, IRepository<Preference> repPref)
        {
            _promoCodeRepository = repP;
            _roleRepository = repRole;
            _efPromo = efPromo;
            _prefRepository = repPref;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var allPromocodes = await _promoCodeRepository.GetAllAsync();

            List<PromoCodeShortResponse> result = new List<PromoCodeShortResponse>();

            foreach (var pro in allPromocodes)
            {
                result.Add(new PromoCodeShortResponse()
                {
                    Id = pro.id,
                    Code = pro.Code,
                    ServiceInfo = pro.ServiceInfo,
                    BeginDate = pro.BeginDate.ToString(),
                    EndDate = pro.EndDate.ToString(),
                    PartnerName = pro.PartnerName,
                });
            }

            return result;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var allPref = await _prefRepository.GetAllAsync();
            var pref = allPref.FirstOrDefault(s => s.Name == request.Preference);

            // Создание промокода
            var newPromoCode = new PromoCode()
            {
                id = new Guid(),
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Preference = pref,
            };

            await _efPromo.CreateAndGivePromocodeAsync(newPromoCode);

            return Ok();

        }
    }
}