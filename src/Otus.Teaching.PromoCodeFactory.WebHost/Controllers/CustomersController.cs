﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EfDB;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        IRepository<Customer> _efRepositoryCustomers;
        IRepository<Preference> _efRepositoryPreference;


        public CustomersController(IRepository<Customer> repC, IRepository<Preference> repP)
        {
            _efRepositoryCustomers = repC;
            _efRepositoryPreference = repP;
        }

        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var allList = await _efRepositoryCustomers.GetAllAsync();

            List<CustomerShortResponse> response = new List<CustomerShortResponse>();

            foreach (var c in allList)
            {
                response.Add(
                    new CustomerShortResponse()
                    {
                        Id = c.id,
                        FirstName = c.FirstName,
                        LastName = c.LastName,
                        Email = c.Email
                    }
                );
            }

            return response;
        }
        
        /// <summary>
        /// Получение клиента вместе с выданными ему промомкодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
 
            var oneCustomer = await _efRepositoryCustomers.GetByIdAsync(id);

            CustomerResponse response = new CustomerResponse()
            {
                Id = oneCustomer.id,
                FirstName = oneCustomer.FirstName,
                LastName = oneCustomer.LastName,
                Email = oneCustomer.Email,
                Preferences = new List<string>(oneCustomer.Preferences.Select(p => p.Name).ToArray()),
                PromoCodes = new List<string>(oneCustomer.PromoCodes.Select(p => p.id).ToArray().Select(s => s.ToString())),

            };

            return response;

        }
        
        /// <summary>
        /// Создание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {

            List<Preference> addPref = new List<Preference>() ;

            foreach(var p in request.PreferenceIds)
            {
                addPref.Add(_efRepositoryPreference.GetByIdAsync(p).Result);
            }

            var newCustomer = new Customer()
            {
                id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = addPref
            };

            await _efRepositoryCustomers.AddObjectAsync(newCustomer);

            return Ok();

        }
        
        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {

            List<Preference> addPref = new List<Preference>();

            foreach (var p in request.PreferenceIds)
            {
                addPref.Add(_efRepositoryPreference.GetByIdAsync(p).Result);
            }


            Customer updateObject = _efRepositoryCustomers.GetByIdAsync(id).Result;

            updateObject.FirstName = request.FirstName;
            updateObject.LastName = request.LastName;
            updateObject.Email = request.Email;
            updateObject.Preferences = addPref;

            await _efRepositoryCustomers.UpdateObjectAsync(updateObject);

            return Ok();
        }
        
        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {

            await _efRepositoryCustomers.DelObjectAsync(id);

            return Ok();
        }
    }
}